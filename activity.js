//Solution for s29
db.users.find()
//OBJ 2
//db.users.find({ $or: [{firstName: { $regex: 'S', $options: '$i' }}, {lastName: { $regex: 'D', $options: '$i' }}]});
db.users.find({ $or: [{firstName: { $regex: 'S', $options: '$i' }}, {lastName: { $regex: 'D', $options: '$i' }}]}, {firstName: 1, lastName: 1, _id:0});

//Obj 3
db.users.find()
db.users.find({$and: [{department: "HR"}, {age: {$gte: 70}}]});
//obj 4
db.users.find()
db.users.find({$and: [{firstName: { $regex: 'E', $options: '$i' }}, {age: {$lte: 30}}]});